-- file : ~/.xmonad/xmonad.hs

-- import System.IO

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run  -- spawnPipe, hPutStrLn
import XMonad.Util.EZConfig  --additionalKeys

-- WS
import qualified XMonad.StackSet as W
import XMonad.Util.WorkspaceCompare
import XMonad.Actions.CycleWS


main :: IO()
main = do
    myStatusBar <- spawnPipe "~/.cabal/bin/xmobar"
    xmonad $ defaultConfig{
        modMask     = myModMask,  -- mod1Mask : Alt,  mod4Mask : Super
        terminal    = "urxvt -e tmux",
        borderWidth = 1,
        layoutHook  = myLayoutHook,
        manageHook  = myManageHook,
        logHook     = myLogHook myStatusBar,
	startupHook = myStartupHook
    }`additionalKeysP` myAdditionalKeys

myModMask = mod4Mask
myLayoutHook = avoidStruts $ layoutHook defaultConfig
myManageHook = manageDocks <+> manageHook defaultConfig
myLogHook h = dynamicLogWithPP xmobarPP {
    ppOutput = hPutStrLn h
}

myAdditionalKeys =
  [
   -- key binding
    ("C-M-n", moveTo Next NonEmptyWS)
   ,("C-M-p", moveTo Prev NonEmptyWS)
   ,("M-S-n", do t <- findWorkspace getSortByIndex Next EmptyWS 1
                 (windows . W.shift) t
                 (windows . W.greedyView) t
    )
   ,("M-S-p", shiftTo Prev EmptyWS)
  ]

myStartupHook = do
	spawn "emacs"
        spawn "urxvt -e tmux"


